export const defaultSettings: GameSettings = {
  playerCounts: [3, 4, 5, 6],
};

export const validPlayerCounts = [3, 4, 5, 6];

export function playerCounts(settings: GameSettings): number[] {
  return validPlayerCounts.filter((c) => settings.playerCounts.includes(c));
}
