import { crossfade } from "svelte/transition";
import { fade } from "svelte/transition";
export let [send, receive] = crossfade({
  duration: (d) => Math.sqrt(d * 500),
  fallback: fade
});
