export function withUserLast(
  players: Player[],
  userId: PlayerId | null
): Player[] {
  const userIdx = players.findIndex((p) => p.id === userId);
  if (userId === null || userIdx === -1) {
    return players;
  } else {
    return [...players.slice(userIdx + 1), ...players.slice(0, userIdx + 1)];
  }
}

export function formatMoney(value: number): string {
  return value.toString().replace(/(\d)(?=(\d\d\d)+($|\.))/, "$1,");
}

export function isActive(phase: Phase, playerId: PlayerId): boolean {
  switch (phase.name) {
    case "buying":
      return phase.player === playerId;
    case "buyingComplete":
      return phase.ready[playerId] !== true;
    case "selling":
      const sale = phase.sale ?? {};
      return sale[playerId] === undefined;
    case "sellingComplete":
      return phase.ready[playerId] !== true;
    case "gameComplete":
      return false;
  }
}

export function isSpeaking(phase: Phase, playerId: PlayerId): boolean {
  switch (phase.name) {
    case "buying":
    case "buyingComplete":
      return !!phase.purchases[playerId];

    case "selling":
    case "sellingComplete":
    case "gameComplete":
      return false;
  }
}

export function passingPrice(bid: number = 0) {
  return Math.ceil(bid / 2000) * 1000;
}

function getSalesSum(player: Player): number {
  const sales = player.sales ?? [];
  return sales
    .map(([property, currency]) => currency)
    .map(getCashValue)
    .reduce((a, b) => a + b, 0);
}

export type PlayerSummary = Player & { salesSum: number; total: number; rankValue: number };
export function sortedByRank(players: Player[]): PlayerSummary[] {
  const summaries = players.map((p) => {
    const salesSum = getSalesSum(p);
    const total = p.cash + salesSum;
    const rankValue = total * 1000 + p.cash;
    return { salesSum, total, rankValue, ...p, };
  });

  return summaries.sort((a, b) => b.rankValue - a.rankValue);
}

export function sortedByValue(cards: number[]): number[] {
  return cards.sort((a, b) => a - b);
}
export function sortedCompletedSalesByPropertyValue(
  sales: [any, [PropertyCard, any]][]
): [any, [PropertyCard, any]][] {
  return sales.sort((a, b) => a[1][0] - b[1][0]);
}

export function getCashValue(card: CurrencyCard) {
  const idx = Math.floor((card - 31) / 2);
  if (idx === 0) {
    return 0;
  } else {
    return (idx + 1) * 1000;
  }
}
