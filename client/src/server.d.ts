type PlayerId = number;
type GameState = {
  players: Player[];
  phase: Phase;
};

type PropertyCard = number;
type CurrencyCard = number;
type Cash = number;
type Purchase = [PropertyCard, Cash];

type BuyingPhase = {
  name: "buying";
  player: PlayerId;
  propertyDeck: PropertyCard[];
  properties: PropertyCard[];
  bids: Record<PlayerId, Cash>;
  purchases: Record<PlayerId, Purchase>;
};

type Phase =
  | BuyingPhase
  | {
      name: "buyingComplete";
      player: PlayerId;
      propertyDeck: PropertyCard[];
      ready: Record<PlayerId, boolean>;
      purchases: Record<PlayerId, Purchase>;
    }
  | {
      name: "selling";
      sale: Record<PlayerId, PropertyCard>;
      currencyDeck: CurrencyCard[];
      currencies: CurrencyCard[];
    }
  | {
      name: "sellingComplete";
      ready: Record<PlayerId, boolean>;
      currencyDeck: CurrencyCard[];
      saleResult: Record<PlayerId, [PropertyCard, CurrencyCard]>;
    }
  | {
      name: "gameComplete";
    };

type Player = {
  id: PlayerId;
  cash: Cash;
  cards: PropertyCard[];
  sales?: [PropertyCard, CurrencyCard][];
};

type GameSettings = {
  playerCounts: number[];
};

type Action =
  | { name: "bid"; cash: Cash }
  | { name: "pass" }
  | { name: "sell"; card: PropertyCard }
  | { name: "ready" };
