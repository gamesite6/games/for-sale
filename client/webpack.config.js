const path = require("path");
const sveltePreprocess = require("svelte-preprocess");

const production = process.env.NODE_ENV === "production";

module.exports = {
  entry: path.resolve(__dirname, "src", "index.ts"),
  mode: production ? "production" : "development",
  devtool: production ? "source-map" : "eval-source-map",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
    libraryTarget: "commonjs2",
  },
  resolve: {
    extensions: [".ts", ".mjs", ".js", ".svelte"],
    mainFields: ["svelte", "browser", "module", "main"],
  },
  module: {
    rules: [
      { test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/ },
      { test: /\.(png|svg)$/i, use: "url-loader" },
      {
        test: /\.(html|svelte)$/,
        exclude: /node_modules/,
        use: {
          loader: "svelte-loader",
          options: {
            preprocess: sveltePreprocess(),
          },
        },
      },
    ],
  },
};
