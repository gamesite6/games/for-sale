import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/house-flip/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "house-flip",
      name: "Gamesite6_HouseFlip",
    },
  },
  plugins: [svelte()],
});
