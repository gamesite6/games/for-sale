package game

import "errors"

type ActionJSON struct {
	Name string        `json:"name"`
	Cash *Cash         `json:"cash,omitempty"`
	Card *PropertyCard `json:"card,omitempty"`
}

func (json ActionJSON) ToAction() (Action, error) {
	switch json.Name {
	case "bid":
		return Bid{*json.Cash}, nil
	case "pass":
		return Pass{}, nil
	case "ready":
		return Ready{}, nil
	case "sell":
		return Sell{*json.Card}, nil
	default:
		return nil, errors.New("invalid action json")
	}
}

type Action interface {
	ToJSON() ActionJSON
}

type Bid struct {
	Cash Cash
}

func (bid Bid) ToJSON() ActionJSON {
	return ActionJSON{
		Name: "bid",
		Cash: &bid.Cash,
	}
}

type Pass struct{}

func (pass Pass) ToJSON() ActionJSON {
	return ActionJSON{
		Name: "pass",
	}
}

type Sell struct {
	Card PropertyCard
}

func (sell Sell) ToJSON() ActionJSON {
	return ActionJSON{
		Name: "sell",
		Card: &sell.Card,
	}
}

type Ready struct {
}

func (ready Ready) ToJSON() ActionJSON {
	return ActionJSON{
		Name: "ready",
	}
}
