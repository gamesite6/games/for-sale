package game

import "math/rand"

type PropertyCard int

func NewPropertyDeck(playerCount int) []PropertyCard {
	deck := make([]PropertyCard, 30)
	for i := range deck {
		deck[i] = PropertyCard(i + 1)
	}
	shufflePropertyDeck(deck)

	if playerCount == 3 {
		deck = deck[6:]
	} else if playerCount == 4 {
		deck = deck[2:]
	}

	return deck
}

func shufflePropertyDeck(deck []PropertyCard) {
	rand.Shuffle(len(deck), func(i, j int) {
		deck[i], deck[j] = deck[j], deck[i]
	})
}

func TakeLowestProperty(cards []PropertyCard) (PropertyCard, []PropertyCard, bool) {
	lowest := PropertyCard(99)
	lowestIndex := -1

	for i, card := range cards {
		if card < lowest {
			lowest = card
			lowestIndex = i
		}
	}

	if lowestIndex == -1 {
		return lowest, cards, false
	}

	cards = append(cards[:lowestIndex], cards[lowestIndex+1:]...)
	return lowest, cards, true
}

func RemoveProperty(cards []PropertyCard, property PropertyCard) ([]PropertyCard, bool) {
	propertyIndex := -1

	for i, card := range cards {
		if card == property {
			propertyIndex = i
		}
	}

	if propertyIndex == -1 {
		return cards, false
	}

	cards = append(cards[:propertyIndex], cards[propertyIndex+1:]...)
	return cards, true
}
