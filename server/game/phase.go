package game

import (
	"errors"
	"sort"
)

type PhaseJSON struct {
	Name         string                     `json:"name"`
	Player       *PlayerID                  `json:"player,omitempty"`
	Properties   *[]PropertyCard            `json:"properties,omitempty"`
	PropertyDeck *[]PropertyCard            `json:"propertyDeck,omitempty"`
	Ready        *map[PlayerID]bool         `json:"ready,omitempty"`
	Bids         *map[PlayerID]Cash         `json:"bids,omitempty"`
	Purchases    *map[PlayerID]Purchase     `json:"purchases,omitempty"`
	Currencies   *[]CurrencyCard            `json:"currencies,omitempty"`
	CurrencyDeck *[]CurrencyCard            `json:"currencyDeck,omitempty"`
	Sale         *map[PlayerID]PropertyCard `json:"sale,omitempty"`
	SaleResult   *map[PlayerID]Sale         `json:"saleResult,omitempty"`
}

func (json PhaseJSON) ToPhase() (Phase, error) {
	switch json.Name {
	case "buying":
		return Buying{
			Player:       *json.Player,
			Properties:   *json.Properties,
			PropertyDeck: *json.PropertyDeck,
			Bids:         *json.Bids,
			Purchases:    *json.Purchases,
		}, nil

	case "buyingComplete":
		return BuyingComplete{
			Player:       *json.Player,
			PropertyDeck: *json.PropertyDeck,
			Purchases:    *json.Purchases,
			Ready:        *json.Ready,
		}, nil

	case "selling":
		return Selling{
			Currencies:   *json.Currencies,
			CurrencyDeck: *json.CurrencyDeck,
			Sale:         *json.Sale,
		}, nil

	case "sellingComplete":
		return SellingComplete{
			CurrencyDeck: *json.CurrencyDeck,
			SaleResult:   *json.SaleResult,
			Ready:        *json.Ready,
		}, nil

	case "gameComplete":
		return GameComplete{}, nil

	default:
		return nil, errors.New("invalid phase json")
	}
}

type Phase interface {
	ToJSON() PhaseJSON
}

type Buying struct {
	Player       PlayerID
	Properties   []PropertyCard
	PropertyDeck []PropertyCard
	Bids         map[PlayerID]Cash
	Purchases    map[PlayerID]Purchase
}

func NewBuyingPhase(deck []PropertyCard, startingPlayer PlayerID, playerCount int) (Buying, error) {
	if len(deck) < playerCount {
		return Buying{}, errors.New("out of property cards")
	}

	props := deck[:playerCount]
	deck = deck[playerCount:]

	return Buying{
		Player:       startingPlayer,
		Properties:   props,
		PropertyDeck: deck,
		Bids:         make(map[PlayerID]Cash),
		Purchases:    make(map[PlayerID]Purchase),
	}, nil
}

func (phase Buying) ToJSON() PhaseJSON {
	return PhaseJSON{
		Name:         "buying",
		Player:       &phase.Player,
		Properties:   &phase.Properties,
		PropertyDeck: &phase.PropertyDeck,
		Bids:         &phase.Bids,
		Purchases:    &phase.Purchases,
	}
}

type Purchase [2]interface{}

func NewPurchase(property PropertyCard, cash Cash) Purchase {
	return [2]interface{}{property, cash}
}

type BuyingComplete struct {
	Player       PlayerID
	PropertyDeck []PropertyCard
	Purchases    map[PlayerID]Purchase
	Ready        map[PlayerID]bool
}

func NewBuyingCompletePhase(
	winningBidder PlayerID,
	deck []PropertyCard,
	purchases map[PlayerID]Purchase,
) BuyingComplete {
	return BuyingComplete{
		Player:       winningBidder,
		PropertyDeck: deck,
		Purchases:    purchases,
		Ready:        make(map[PlayerID]bool),
	}
}
func (phase BuyingComplete) ToJSON() PhaseJSON {
	return PhaseJSON{
		Name:         "buyingComplete",
		Player:       &phase.Player,
		PropertyDeck: &phase.PropertyDeck,
		Ready:        &phase.Ready,
		Purchases:    &phase.Purchases,
	}
}

type Selling struct {
	Currencies   []CurrencyCard
	CurrencyDeck []CurrencyCard
	Sale         map[PlayerID]PropertyCard
}

func NewSellingPhase(deck []CurrencyCard, playerCount int) (Selling, error) {
	if len(deck) < playerCount {
		return Selling{}, errors.New("out of currency cards")
	}

	currencies := deck[:playerCount]
	deck = deck[playerCount:]

	return Selling{
		Currencies:   currencies,
		CurrencyDeck: deck,
		Sale:         make(map[PlayerID]PropertyCard),
	}, nil

}

func (phase Selling) ToJSON() PhaseJSON {
	return PhaseJSON{
		Name:         "selling",
		Currencies:   &phase.Currencies,
		CurrencyDeck: &phase.CurrencyDeck,
		Sale:         &phase.Sale,
	}
}

type SellingComplete struct {
	CurrencyDeck []CurrencyCard
	SaleResult   map[PlayerID]Sale
	Ready        map[PlayerID]bool
}

func NewSellingCompletePhase(sellingPhase Selling) (SellingComplete, error) {
	currencies := sellingPhase.Currencies
	sort.Slice(currencies, func(i, j int) bool {
		return currencies[i] < currencies[j]
	})

	type PlayerProperty struct {
		playerID PlayerID
		property PropertyCard
	}
	var playerProperties = make([]PlayerProperty, 0)
	for playerID, property := range sellingPhase.Sale {
		playerProperties = append(playerProperties, PlayerProperty{playerID, property})
	}
	sort.Slice(playerProperties, func(i, j int) bool {
		return playerProperties[i].property < playerProperties[j].property
	})

	var saleResult = make(map[PlayerID]Sale)
	for i, pp := range playerProperties {
		saleResult[pp.playerID] = Sale{pp.property, currencies[i]}
	}

	return SellingComplete{
		CurrencyDeck: sellingPhase.CurrencyDeck,
		Ready:        make(map[PlayerID]bool),
		SaleResult:   saleResult,
	}, nil
}

func (phase SellingComplete) ToJSON() PhaseJSON {
	return PhaseJSON{
		Name:         "sellingComplete",
		CurrencyDeck: &phase.CurrencyDeck,
		SaleResult:   &phase.SaleResult,
		Ready:        &phase.Ready,
	}
}

type GameComplete struct{}

func (phase GameComplete) ToJSON() PhaseJSON {
	return PhaseJSON{
		Name: "gameComplete",
	}
}
