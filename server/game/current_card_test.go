package game

import "testing"

func TestCurrencyCard(t *testing.T) {
	zero1 := ToCash(31)
	zero2 := ToCash(32)

	if zero1 != zero2 || zero1 != 0 {
		t.Errorf("got %v, %v, expected 0, 0", zero1, zero2)
	}

	got := ToCash(44)
	if got != 7000 {
		t.Errorf("got %v, expected 7000", got)
	}

	got = ToCash(51)
	if got != 11000 {
		t.Errorf("got %v, expected 11000", got)
	}

	max1 := ToCash(59)
	max2 := ToCash(60)

	if max1 != max2 || max1 != 15000 {
		t.Errorf("got %v, %v, expected 15000, 15000", max1, max2)
	}

}
