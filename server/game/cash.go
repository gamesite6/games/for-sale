package game

type Cash int

func PassingPrice(bid Cash) Cash {
	return ((bid + 1000) / 2000) * 1000
}
