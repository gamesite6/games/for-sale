package game

import "math/rand"

type CurrencyCard int

func NewCurrencyDeck(playerCount int) []CurrencyCard {

	deck := make([]CurrencyCard, 30)
	for i := range deck {
		deck[i] = CurrencyCard(i + 1 + 30)
	}

	shuffleCurrencyDeck(deck)

	if playerCount == 3 {
		deck = deck[6:]
	} else if playerCount == 4 {
		deck = deck[2:]
	}

	return deck
}

func ToCash(card CurrencyCard) int {
	idx := (int(card) - 31) / 2

	if idx == 0 {
		return 0
	} else {
		return (idx + 1) * 1000
	}
}

func shuffleCurrencyDeck(deck []CurrencyCard) {
	rand.Shuffle(len(deck), func(i, j int) {
		deck[i], deck[j] = deck[j], deck[i]
	})
}
