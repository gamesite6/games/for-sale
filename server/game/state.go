package game

import (
	"errors"
	"math/rand"
)

type StateJSON struct {
	Players []Player  `json:"players"`
	Phase   PhaseJSON `json:"phase"`
}

func (json StateJSON) ToState() (State, error) {
	var state State

	phase, err := json.Phase.ToPhase()
	if err != nil {
		return state, err
	}
	state.Phase = phase

	state.Players = json.Players
	return state, nil
}

type State struct {
	Players []Player
	Phase   Phase
}

func (state State) ToJSON() StateJSON {
	return StateJSON{
		Players: state.Players,
		Phase:   state.Phase.ToJSON(),
	}
}

func (state State) nextPlayer(userID PlayerID) (*Player, bool) {

	userIndex := -1
	for i, player := range state.Players {
		if player.ID == userID {
			userIndex = i
			break
		}
	}

	if userIndex == -1 {
		return &Player{}, false
	}

	playerCount := len(state.Players)
	switch phase := state.Phase.(type) {
	case Buying:
		for i := 1; i < playerCount; i++ {
			player := &state.Players[(userIndex+i)%playerCount]
			if _, hasPurchased := phase.Purchases[player.ID]; !hasPurchased {
				return player, true
			}
		}
	}

	return &Player{}, false
}

func containsDuplicate(playerIDs []PlayerID) bool {
	var seen map[PlayerID]bool = make(map[PlayerID]bool)
	for _, playerID := range playerIDs {
		if seen[playerID] {
			return true
		}
		seen[playerID] = true
	}
	return false
}

func InitialState(playerIDs []PlayerID, settings Settings, seed int64) (State, error) {
	rand.Seed(seed)

	var playerCount = len(playerIDs)
	var state State

	if containsDuplicate(playerIDs) {
		return state, errors.New("duplicate player id(s)")
	}
	if !contains(settings.PlayerCounts, playerCount) {
		return state, errors.New("invalid player count")
	}

	state.Players = make([]Player, len(playerIDs))
	for i, playerID := range playerIDs {
		var cash Cash
		if playerCount < 5 {
			cash = 18000
		} else {
			cash = 14000
		}
		state.Players[i] = Player{
			ID:    playerID,
			Cash:  cash,
			Cards: make([]PropertyCard, 0),
		}
	}

	var firstPlayer = state.Players[rand.Int()%len(state.Players)]
	var deck = NewPropertyDeck(playerCount)

	phase, err := NewBuyingPhase(
		deck,
		firstPlayer.ID,
		playerCount,
	)
	if err != nil {
		return state, err
	}
	state.Phase = phase

	return state, nil
}

func contains(as []int, b int) bool {
	for _, a := range as {
		if a == b {
			return true
		}
	}
	return false
}

func containsPropertyCard(cards []PropertyCard, x PropertyCard) bool {
	for _, card := range cards {
		if card == x {
			return true
		}
	}
	return false
}

func PerformAction(state State, action Action, userID PlayerID, settings Settings, seed int64) (State, error) {
	if !isAllowed(state, action, userID, settings) {
		return state, errors.New("action not allowed")
	}

	rand.Seed(seed)
	playerCount := len(state.Players)

	user, ok := findPlayer(state.Players, userID)
	if !ok {
		return state, errors.New("player not found")
	}

	switch phase := state.Phase.(type) {
	case Buying:
		switch action := action.(type) {
		case Bid:
			phase.Bids[userID] = action.Cash

			nextPlayer, ok := state.nextPlayer(userID)
			if !ok {
				return state, errors.New("next player not found")
			}
			phase.Player = nextPlayer.ID

			state.Phase = phase
			return state, nil
		case Pass:
			card, cards, ok := TakeLowestProperty(phase.Properties)
			if !ok {
				return state, errors.New("failed to find lowest card")
			}
			user.Cards = append(user.Cards, card)

			phase.Properties = cards

			bid := phase.Bids[userID]
			delete(phase.Bids, userID)
			price := PassingPrice(bid)

			user.Cash -= price

			phase.Purchases[userID] = NewPurchase(card, price)

			nextPlayer, ok := state.nextPlayer(userID)
			if !ok {
				return state, errors.New("next player not found")
			}

			if len(phase.Properties) == 1 {

				winningBid := phase.Bids[nextPlayer.ID]
				card := phase.Properties[0]
				nextPlayer.Cash -= winningBid
				nextPlayer.Cards = append(nextPlayer.Cards, card)
				phase.Purchases[nextPlayer.ID] = NewPurchase(card, winningBid)

				phase := NewBuyingCompletePhase(
					nextPlayer.ID,
					phase.PropertyDeck,
					phase.Purchases,
				)

				state.Phase = phase
				return state, nil
			}

			phase.Player = nextPlayer.ID
			state.Phase = phase
			return state, nil
		}
	case BuyingComplete:
		switch action.(type) {
		case Ready:
			phase.Ready[userID] = true

			allReady := true
			for _, player := range state.Players {
				if !phase.Ready[player.ID] {
					allReady = false
				}
			}

			if allReady {
				if len(phase.PropertyDeck) == 0 {
					currencyDeck := NewCurrencyDeck(playerCount)
					phase, err := NewSellingPhase(currencyDeck, playerCount)
					if err != nil {
						return state, err
					}
					state.Phase = phase
					return state, nil
				}

				phase, err := NewBuyingPhase(
					phase.PropertyDeck,
					phase.Player,
					playerCount,
				)
				if err != nil {
					return state, err
				}
				state.Phase = phase
				return state, nil
			}

			return state, nil
		}

	case Selling:
		switch action := action.(type) {
		case Sell:
			phase.Sale[userID] = action.Card

			user.Cards, _ = RemoveProperty(user.Cards, action.Card)

			if len(phase.Sale) == playerCount {
				var err error
				state.Phase, err = NewSellingCompletePhase(phase)
				if err != nil {
					return state, errors.New("failed to create 'selling complete' phase")
				}
				return state, nil
			}

			state.Phase = phase
			return state, nil
		}
	case SellingComplete:
		switch action.(type) {
		case Ready:
			phase.Ready[userID] = true

			allReady := true
			for _, player := range state.Players {
				if !phase.Ready[player.ID] {
					allReady = false
				}
			}

			if allReady {

				for i := 0; i < playerCount; i++ {
					state.Players[i].Sales = append(
						state.Players[i].Sales,
						phase.SaleResult[state.Players[i].ID],
					)
				}

				if len(phase.CurrencyDeck) == 0 {
					state.Phase = GameComplete{}
					return state, nil
				}

				var err error
				state.Phase, err = NewSellingPhase(phase.CurrencyDeck, playerCount)
				return state, err
			}

			state.Phase = phase
			return state, nil
		}
	}

	return state, errors.New("unhandled phase/action")
}

func isAllowed(state State, action Action, userID PlayerID, settings Settings) bool {
	user, ok := findPlayer(state.Players, userID)
	if !ok {
		return false
	}

	switch phase := state.Phase.(type) {
	case Buying:
		highestBid := findHighestBid(phase.Bids)
		switch action := action.(type) {
		case Bid:
			return phase.Player == userID &&
				user.Cash >= action.Cash &&
				action.Cash > highestBid &&
				action.Cash >= 1000

		case Pass:
			return phase.Player == userID
		}
	case BuyingComplete:
		switch action.(type) {
		case Ready:
			return !phase.Ready[userID]
		}
	case Selling:
		switch action := action.(type) {
		case Sell:
			_, alreadySold := phase.Sale[userID]
			return containsPropertyCard(user.Cards, action.Card) && !alreadySold
		}
	case SellingComplete:
		switch action.(type) {
		case Ready:
			return !phase.Ready[userID]
		}
	}

	return false
}

func findPlayer(players []Player, playerID PlayerID) (*Player, bool) {
	for i := 0; i < len(players); i++ {
		player := &players[i]
		if player.ID == playerID {
			return player, true
		}
	}
	return &Player{}, false
}

func findHighestBid(bids map[PlayerID]Cash) Cash {
	var highestBid Cash = Cash(-1)

	for _, bid := range bids {
		if bid > highestBid {
			highestBid = bid
		}
	}

	return highestBid
}
