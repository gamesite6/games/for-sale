package game

type Settings struct {
	PlayerCounts []int `json:"playerCounts"`
}

func (s Settings) ValidatedPlayerCounts() []int {
	playerCounts := map[int]bool{}
	for _, count := range s.PlayerCounts {
		playerCounts[count] = true
	}

	validPlayerCounts := []int{3, 4, 5, 6}
	result := []int{}

	for _, c := range validPlayerCounts {
		if playerCounts[c] {
			result = append(result, c)
		}
	}

	return result
}
