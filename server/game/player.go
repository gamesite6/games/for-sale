package game

type PlayerID int

type Player struct {
	ID    PlayerID       `json:"id"`
	Cash  Cash           `json:"cash"`
	Cards []PropertyCard `json:"cards"`
	Sales []Sale         `json:"sales,omitempty"`
}

type Sale [2]interface{}

func NewSale(property PropertyCard, currency CurrencyCard) Sale {
	return [2]interface{}{property, currency}
}
