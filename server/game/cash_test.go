package game

import "testing"

func TestPassingPrice(t *testing.T) {
	got := PassingPrice(3000)
	if got != 2000 {
		t.Errorf("got %v, expected 2000", got)
	}

	got = PassingPrice(4000)
	if got != 2000 {
		t.Errorf("got %v, expected 2000", got)
	}

	got = PassingPrice(5000)
	if got != 3000 {
		t.Errorf("got %v, expected 3000", got)
	}
}
