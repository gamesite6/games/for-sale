#!/bin/bash

export PORT=8081

nodemon --watch . --ext "go" --exec "go run main.go || exit 1" --signal SIGTERM
