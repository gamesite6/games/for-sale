package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"gamesite6.com/house-flip/game"
)

func main() {
	var port uint16
	if parsed, err := strconv.ParseUint(os.Getenv("PORT"), 10, 16); err == nil {
		port = uint16(parsed)
	} else {
		panic("env var PORT required")
	}
	url := fmt.Sprintf("0.0.0.0:%d", port)

	http.HandleFunc("/info", func(w http.ResponseWriter, r *http.Request) {
		var req infoReq

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		playerCounts := req.Settings.ValidatedPlayerCounts()
		res := infoRes{playerCounts}

		resBytes, err := json.Marshal(res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(resBytes)
	})

	http.HandleFunc("/initial-state", func(w http.ResponseWriter, r *http.Request) {
		var req initialStateReq

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		state, err := game.InitialState(req.Players, req.Settings, req.Seed)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnprocessableEntity)
			return
		}

		var res = initialStateRes{state.ToJSON()}

		resBytes, err := json.Marshal(res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(resBytes)
	})

	http.HandleFunc("/perform-action", func(w http.ResponseWriter, r *http.Request) {
		var req performActionReq
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		state, err := req.State.ToState()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		action, err := req.Action.ToAction()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		nextState, err := game.PerformAction(state, action, *req.PerformedBy, req.Settings, req.Seed)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnprocessableEntity)
			return
		}
		completed := false // TODO

		res := performActionRes{
			NextState: nextState.ToJSON(),
			Completed: completed,
		}
		resBytes, err := json.Marshal(res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(resBytes)
	})

	log.Printf("Listening on %v\n", url)
	log.Fatal(http.ListenAndServe(url, nil))
}

type infoReq struct {
	Settings game.Settings `json:"settings"`
}

type infoRes struct {
	PlayerCounts []int `json:"playerCounts"`
}

type initialStateReq struct {
	Players  []game.PlayerID `json:"players"`
	Settings game.Settings   `json:"settings"`
	Seed     int64           `json:"seed"`
}

type initialStateRes struct {
	State game.StateJSON `json:"state"`
}

type performActionReq struct {
	Action      game.ActionJSON `json:"action"`
	PerformedBy *game.PlayerID  `json:"performedBy"`
	State       game.StateJSON  `json:"state"`
	Settings    game.Settings   `json:"settings"`
	Seed        int64           `json:"seed"`
}

type performActionRes struct {
	NextState game.StateJSON `json:"nextState"`
	Completed bool           `json:"completed"`
}
